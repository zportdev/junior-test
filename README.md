Hello.

This is a test task for determining skills of junior web developer.

While competing it, you will show your knowledge in both backend and frontend developments.

Using google is allowed. If you got stuck in task for more than 15 minutes, do not hesitate to ask for help.

##Please avoid  asking questions like: 

"I don't know how to do this", 
"I am completely lost, please do this for me".

##Good template for question is:

"While doing {this} and {this} I did this and this, but it is not working as I wanted it to. It works like {this}
and {this} or I receive {this} and {this} errors".

All required software should be preinstalled on the PC you are working on. If you don't see something, please ask before starting test.

##Tasks:

1. Clone this repo to home folder of the PC you are working on.
2. Create a branch from master with some custom name.
3. There is installed apache web server on PC. Please create 2 virtual hosts for 2 projects: backend and frontend. 
   - Backend virtualhost documentRoot should point to backend-app/public/ of current repo.
   - Frontend virtualhost documentRoot should point to frontend-app/ of current repo.
   - We suggest you create virtual hosts with same name, different ports, not to waste time on CORS problem resolving.

4. Develop backend. 
    Current backend is preinstalled symfony 3. However, if you are not familiar with this framework,
    feel free to use the one you know.
    If you are not familiar with any MVC framework, create your own backend, object-oriented design is a must.  
    In the symfony repo, there are 2 empty entities created - player and team.
    - update team entity to have properties: (int) id, (string) name, (string) country, (string) city;
    - update player entity to have properties: (int) id, (string) firstname, (string) lastname, email;
    - connect entities by many-to-one connection (player can have one team, team can have many players);
    - create a database schema according to this entities (via doctrine command);
    - create a controller action with RESTFUL route to GET all teams (JSON format);
    - create a controller action with RESTFUL route to GET all players of the team (e.g /team/1/player);
    - create a controller action with RESTFUL route to POST (create) a new team;
    - create a controller action with RESTFUL route to POST (create) a new player;
    - BONUS task: write a unit test for one of the controllers actions.

    

5. Develop frontend.
	- create a 3-column layout, with fixed navbar on top;
	- navbar should have a search form (input + submit button);
	- add 2 forms to layouts left column: for adding a team and for adding a player;
	- upon submitting this forms, POST request should be done to create team/player;
    - on initialisation, frontend should make a GET request to backend to get all teams.
    - after teams load, a table with these teams and their data should be shown in center column:

    | team name | team country | team city |

    - show team name as a link, upon clicking it, do a request to GET all players;
    - show a list of players as a table in right column;
    - each player should have "profile picture" shown in a row, take this one https://vignette1.wikia.nocookie.net/sote-rp/images/c/c4/User-placeholder.png/revision/latest?cb=20150624004222
    - team table should be sortable by name column (clicking on header of the column);
    - player table should be sortable by email
    - html should not break in mobile view (responsive etc.);
    - BONUS: use twitter bootstrap styles;
    - BONUS: add validation for form inputs (length, email);
    - BONUS: use react or angular;

6. Push your changes to repo.

##TIPS:
    - do as many commits as possible (good practice);
    - use JsonResponse object from Symfony for rest api response;
